var myApp = angular.module('myApp', ['ngRoute', 'ngMaterial']);

myApp.config(function($routeProvider, $mdIconProvider) {
  
    $mdIconProvider
    .iconSet("coreSVG", 'icons.svg', 24) 

    $routeProvider

    .when('/', {
        templateUrl: 'pages/start_page.html',
        controller: 'mainController'
    })
});

myApp.controller('mainController', ['$scope', '$mdDialog','$mdToast','$mdSidenav', function ($scope, $mdDialog, $mdToast, $mdSidenav) {

    /*  Dialogs types */

    $scope.showAlert = function(ev) {
        // Appending dialog to document.body to cover sidenav in docs app
        // Modal dialogs should fully cover application
        // to prevent interaction outside of dialog
        console.log('btn clicked');

        $mdDialog.show(
          $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title('This is an alert title')
            .textContent('You can specify some description text in here.')
            .ariaLabel('Alert Dialog Demo')
            .ok('Got it!')
            .targetEvent(ev)
        );
    };

    $scope.showPrompt = function(ev) {
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.prompt()
            .title('What would you name your dog?')
            .textContent('Bowser is a common name.')
            .placeholder('Dog name')
            .ariaLabel('Dog name')
            .initialValue('Buddy')
            .targetEvent(ev)
            .ok('Okay!')
            .cancel('I\'m a cat person');

        $mdDialog.show(confirm).then(function(result) {
            $scope.status = 'You decided to name your dog ' + result + '.';
        }, function() {
            $scope.status = 'You didn\'t name your dog.';
        });
    };

    $scope.showAdvanced = function() {
        $mdDialog.show({
            controller: DialogController,
            templateUrl: 'pages/custom_dialog.html',
            parent: angular.element(document.body),
            clickOutsideToClose:true,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        }).then(function(answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                $scope.status = 'You cancelled the dialog.';
        });
    };

    function DialogController($scope, $mdDialog) {
        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);
        };
    }

    /*  Snackbar */

    var last = {
        bottom: false,
        top: true,
        left: false,
        right: true
      };

    $scope.toastPosition = angular.extend({},last);

    $scope.getToastPosition = function() {
        var current = $scope.toastPosition;

        if ( current.bottom && last.top ) current.top = false;
        if ( current.top && last.bottom ) current.bottom = false;
        if ( current.right && last.left ) current.left = false;
        if ( current.left && last.right ) current.right = false;
    
        last = angular.extend({},current);
    
        return Object.keys($scope.toastPosition)
          .filter(function(pos) { return $scope.toastPosition[pos]; })
          .join(' ');
      };

      $scope.showSimpleToast = function() {
        var pinTo = $scope.getToastPosition();
    
        $mdToast.show(
          $mdToast.simple()
            .textContent('Hello Igor! the best squat trainer ever!')
            .position(pinTo )
            .hideDelay(5000)
        );
      };

    /*  Sidenav */

    $scope.toggleLeft = buildToggler('right');

    function buildToggler(componentId) {
        return function() {
            $mdSidenav(componentId).toggle();
        };
    }

    /* Menu */

    var originatorEv;

    $scope.openMenu = function($mdMenu, ev) {

      originatorEv = ev;
      $mdMenu.open(ev);
    };

    /* List */

    $scope.todos = [
        {
            what: 'Brunch this weekend?',
            who: 'Min Li Chan',
            when: '3:08PM',
            notes: " I'll be in your neighborhood doing errands"
        },
        {
            what: 'Brunch this weekend?',
            who: 'Min Li Chan',
            when: '3:08PM',
            notes: " I'll be in your neighborhood doing errands"
        }
    ];
}])
