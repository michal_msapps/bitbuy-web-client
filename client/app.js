var myApp = angular.module('myApp', ['ngRoute', 'ngMaterial', 'swxSessionStorage', 'ngSanitize', 'infinite-scroll']);

const baseUrl = "https://obscure-beach-67052.herokuapp.com";

myApp.config(function($routeProvider) {

    $routeProvider

        .when('/', {
            templateUrl: 'pages/secondPage2.html',
            controller: 'secondController'
        })

        .when('/userItems', {
            templateUrl: 'pages/userItems.html',
            controller: 'userController'
        })

        .when('/buyerBoard', {
            templateUrl: 'pages/buyerBoard.html',
            controller: 'candidateController'
        })

        .when('/profileInfo', {
            templateUrl: 'pages/userProfile.html',
            controller: 'userController'
        })

        .when('/addItem', {
            templateUrl: 'pages/addItem.html',
            controller: 'userController'
        })

        .when('/updateItem', {
            templateUrl: 'pages/updateItem.html',
            controller: 'userController'
        })

});

myApp.service('itemService', function() {
    this.item;

    this.setItem = function (item) {
        this.item = item;
    }

    this.getItem = function () {
        return this.item
    }
});

myApp.service('chatService', function ($timeout) {

    this.ref;

    var database = firebase.database();
    var messagesRef = database.ref('messages');
    var user = firebase.auth().currentUser;

    this.loadMessages = function () {

        database.ref(this.ref).off();

        var setMessage = function (data) {
            appendMsg(data.val());
            console.log(data.val());
        }

        database.ref(this.ref).on('child_added', setMessage);
    }

    this.postMessage = function (message) {
        messagesRef = database.ref(this.ref);
        messagesRef.push({
            name: user.displayName,
            text: message
        })
    }

    function appendMsg(message){

        $timeout(function() {

            var messageContainer = document.getElementById('messages');

            var container = document.createElement('div');
            container.setAttribute('class', 'message-container');

            var m = '<div class="spacing"><div class="pic"></div></div>' +
                '<div class="message">'+ message.text +'</div>' +
                '<div class="name">'+ message.name +'</div>';

            container.innerHTML = m;
            messageContainer.appendChild(container);
            messageContainer.scrollTop = messageContainer.scrollHeight;
        })
    }

})

myApp.controller('navCtrl', ['$scope','$sessionStorage','$window','$http','$mdDialog', '$mdSidenav', '$sce', 'itemService', function ($scope, $sessionStorage, $window, $http, $mdDialog, $mdSidenav, $sce, itemService) {

    var initialOptions = {
        loginBtn: false,
        userOptions: true
    };

    // Get a reference to the storage service, which is used to create references in your storage bucket
    var storage = firebase.storage();
    // Create a storage reference from our storage service
    var storageRef = storage.ref();

    $scope.itemsArr = {};

    $scope.viewItem = function(item) {

        if(! $sessionStorage.get('userToken')){
            $mdDialog.show($mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('')
                .textContent('Sign-in to BitBuy to view this item')
                .ariaLabel('Alert Dialog Demo')
                .ok('Dismiss')
            );
        } else {

            var idToken = $sessionStorage.get('userToken');
            console.log(item.userID);

            var userItems = $sessionStorage.get('userItems');
            console.log(userItems);

            if(! userItems.find(i => i._id == item._id)){
                $scope.applyToBuyVisibility = true;
                $scope.CandidateAndRemoveVisibility = false;
            } else {
                $scope.applyToBuyVisibility = false;
                $scope.CandidateAndRemoveVisibility = true;
            }

            $scope.applyToBuy = function () {

                console.log(item._id);

                $http({
                    method: 'POST',
                    url: baseUrl + '/applyToBuy',
                    data: {userID: item.userID, itemID: item._id},
                    headers: {'x-auth': idToken}
                }).then(function successCallBack(response) {
                    console.log(response.data);

                    $mdDialog.show($mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('Apply to buy')
                        .textContent('Your request to buy the item has been sent.')
                        .ariaLabel('Alert Dialog Demo')
                        .ok('Dismiss')
                    );

                }, function failureCallBack(err) {

                    if(err.data.code == 11200){
                        $mdDialog.show($mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('ERROR')
                            .textContent('Your have already sent a request to buy the item')
                            .ariaLabel('Alert Dialog Demo')
                            .ok('Dismiss')
                        );
                    }
                    console.log(err.data);

                });
            }

            $scope.startChat = function (ev) {
                $mdDialog.show({
                    controller: chatDialogController,
                    templateUrl: 'pages/chatDialog.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false,
                    fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
                }).then(function (answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function () {
                    $scope.status = 'You cancelled the dialog.';
                });

                function chatDialogController($scope, $mdDialog, $timeout, chatService) {

                    var user = firebase.auth().currentUser;
                    chatService.ref = 'messages/' + item._id + '/' + item.userID + '-' + user.uid;

                    $scope.load = function () {
                        chatService.loadMessages();
                    }

                    $scope.postMsg = function(){
                        chatService.postMessage($scope.msgInput);
                        $scope.msgInput = '';
                    }

                    $scope.endChat = function () {
                        $mdDialog.hide()
                    }
                }
            }


            $http({
                method: 'POST',
                url: baseUrl + '/getUserRatingAvg',
                data: {userID: item.userID},
                headers: {'x-auth': idToken}
            }).then(function successCallback(response) {

                console.log(response.data);

                $scope.username = response.data.name;
                $scope.avgUser = response.data.averageRating;

                $scope.userText = '<h6><span class="item-sidenav-detail">User:</span> '+ $scope.username +'</h6><br>';
                $scope.ratingText = '<h6><span class="item-sidenav-detail">Rating:</span> '+ $scope.avgUser +'</h6><br>';

            }, function errorCallback(response) {

                console.log("error get messages");
            });

            console.log(item);
            $scope.itemID = item._id;
            $scope.itemName = item.name;
            $scope.itemPrice = item.price;

            $mdSidenav('main-sidenav').toggle(); // toggle the sidenav
        }

        $scope.chooseCandidate = function() {

            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'pages/custom_dialog.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            }).then(function (answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function () {
                $scope.status = 'You cancelled the dialog.';
            });

        };

        $scope.removeItem = function() {

            var confirm = $mdDialog.confirm()
                .title('Confirm candidacy')
                .textContent('Do you really want to remove your item?')
                .ariaLabel('Lucky day')
                .ok('Yes')
                .clickOutsideToClose(true)
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {


                $http({
                    method: 'POST',
                    url: baseUrl + '/deleteItem',
                    data:{itemID: item._id},
                    headers:{'x-auth': idToken}
                }).then(function successCallBack(response) {

                    console.log(response.data);


                    // Create a reference to the file to delete
                    var desertRef = storageRef.child('/user/'+ item.userID + '/' + item.picture.name);
                    // Delete the file
                    desertRef.delete().then(function() {
                        console.log('deleted image');
                    }).catch(function(error) {
                        console.log(err);
                    });

                    function findItemToRemove(element) {
                        return element._id === response.data._id;
                    }

                    var indexToRemove = $scope.itemsArr.findIndex(findItemToRemove); // the index to remove the item from the array

                    $scope.itemsArr.splice(indexToRemove, 1);

                    userItems = $sessionStorage.get('userItems');
                    var removePos = userItems.findIndex(findItemToRemove);
                    userItems.splice(removePos, 1);
                    $sessionStorage.put('userItems', userItems);

                    $mdSidenav('main-sidenav').toggle(); // toggle sidenav

                }, function failureCallBack(err) {
                    console.log(err);
                })

            }, function() {

            });

        };

        function DialogController($scope, $mdDialog, $http) {

            var prevScope = $scope.$$prevSibling; // get ref to the navCtrl $scope
            console.log(prevScope.itemsArr);

            $http({
                method: 'POST',
                url: baseUrl + '/getCandidates',
                data:{itemID: item._id},
                headers:{'x-auth': idToken}
            }).then(function successCallBack(response) {

                console.log(response.data);

                if(response.data){
                    $scope.candidatesArr = response.data;

                    if($scope.candidatesArr.length > 0){
                        $scope.title = 'Candidates who wants to buy the item';
                    } else {
                        $scope.title = 'There are no candidates for the item';
                    }
                }

            }, function failureCallBack() {

            })

            $scope.selectCandidate = function (candidate) {

                var confirm = $mdDialog.confirm()
                    .title('Confirm candidacy')
                    .textContent('Do you confirm that ' + candidate.candidateName + ' will buy the item')
                    .ariaLabel('Lucky day')
                    .ok('Yes, I confirm')
                    .cancel('Cancel');

                $mdDialog.show(confirm).then(function() {

                    console.log(prevScope.itemsArr);

                    $http({
                        method: 'POST',
                        url: baseUrl + '/selectCandidate',
                        data:{itemID: item._id, candidateID: candidate.candidateID},
                        headers:{'x-auth': idToken}
                    }).then(function successCallBack(response) {

                        console.log(response.data);

                        function findItemToRemove(element) {
                            return element._id === item._id
                        }

                        var indexToRemove = prevScope.itemsArr.findIndex(findItemToRemove); // the index to remove the item from the array

                        prevScope.itemsArr.splice(indexToRemove, 1);

                    }, function failureCallBack() {

                    })

                }, function() {

                });
            }


            $scope.hide = function() {
                $mdDialog.hide();
            };

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };

        }

        $scope.updateItemPage = function () {
            itemService.setItem(item);
            $mdSidenav('main-sidenav').toggle(); // toggle sidenav
            $window.location.href = '#updateItem';
        }

    };

    var ui;

    if($sessionStorage.get('headerBtnStatus')){

        var options = $sessionStorage.get('headerBtnStatus');

        $scope.loginBtn = options['loginBtn'];
        $scope.userOptions = options['userOptions'];
        $scope.notificationsHeader = options['notificationsHeader'];
        $scope.addItemHeader = options['addItemHeader'];
    } else{
        $scope.loginBtn = true;
        $scope.userOptions = false;
        $scope.notificationsHeader = false;
        $scope.addItemHeader = false;
    }

    $scope.logOut = function (ev) {
        firebase.auth().signOut().then(function() {

            $scope.loginBtn = true;
            $scope.userOptions = false;
            $scope.notificationsHeader = false;
            $scope.addItemHeader = false;

            $('.modal').removeData();

            $sessionStorage.remove('headerBtnStatus');
            $sessionStorage.remove('userItems');
            $sessionStorage.remove('userToken');

            ui.reset();
            ui.start('.modal-body', uiConfig);

        }).catch(function(error) {
            // An error happened.
        });
    }

    $scope.myItems = function (ev) {
        $window.location.href = '#userItems';
    }
    $scope.buyerBoard = function (ev) {
        $window.location.href = '#buyerBoard';
    }
    $scope.myProfile = function (ev) {
        $window.location.href = '#profileInfo';
    }
    $scope.searchItem = function (searchField) {

        var item = document.getElementById('itemsScreen');
        if(item){
            item.style.height = (window.innerHeight - 100)+ 'px';
        }

        $http({
            url: baseUrl + '/testSearch',
            method: 'GET',
            params: {search: searchField}
        }).then(function successCallBack(response) {
            console.log(response.data);
            $scope.itemsArr = response.data;
        }, function failureCallBack(err) {
            console.log(err);
        })
    }
    $scope.homePage = function () {
        $window.location.href = '#/';
        $window.location.reload();
    }

    /* Menu */

    var originatorEv;

    $scope.openMenu = function($mdMenu, ev) {

        originatorEv = ev;
        $mdMenu.open(ev);
    };

    // FirebaseUI config.
    var uiConfig = {

        callbacks: {
            signInSuccess: function(currentUser) {

                 firebase.auth().currentUser.getIdToken(/* forceRefresh */ true).then(function(idToken) {

                     console.log(idToken);

                    $http({
                        method : "POST",
                        url : baseUrl + "/userReg",
                        headers: {'x-auth': idToken}
                    }).then(function success(response) {

                        if(response.status == 200){

                            $(function () {
                                $('.modal').modal('toggle');
                            });

                            console.log(currentUser);

                            var btnHeaderStatus = {
                                loginBtn: false,
                                userOptions: true,
                                notificationsHeader: true,
                                addItemHeader: true
                            }

                            $http({
                                method: 'GET',
                                url: baseUrl + '/getUserItems',
                                headers: {'x-auth': idToken}
                            }).then(function successCallBack(response) {

                                $sessionStorage.put('userItems', response.data);
                                console.log(response.data);

                            }, function failureCallBack(err) {
                                console.log(err);
                            })

                            $sessionStorage.put('userToken', idToken);
                            $sessionStorage.put('headerBtnStatus', btnHeaderStatus);

                            $scope.loginBtn = btnHeaderStatus.loginBtn;
                            $scope.userOptions = btnHeaderStatus.userOptions;
                            $scope.notificationsHeader = btnHeaderStatus.notificationsHeader;
                            $scope.addItemHeader = btnHeaderStatus.addItemHeader;

                            // $window.location.href = '#secondPage2';

                        }

                    }, function myError(response) {

                        firebase.auth().signOut().then(function() {

                            console.log(response);
                            ui.reset();
                            ui.start('.modal-body', uiConfig);
                            $('.modal').modal('toggle');


                            $mdDialog.show($mdDialog.alert()
                                    .parent(angular.element(document.querySelector('#popupContainer')))
                                    .clickOutsideToClose(true)
                                    .title('ERROR')
                                    .textContent('Please try again later')
                                    .ariaLabel('Alert Dialog Demo')
                                    .ok('Dismiss')
                            );

                        }).catch(err => {

                        });


                        return false;

                        // Handle error registration from server
                    });

                }).catch(function(error) {
                    // Handle error getting token
                });

            },
            uiShown: function() {

            }
        },
        signInFlow: 'popup',
        signInOptions: [
            // Leave the lines as is for the providers you want to offer your users.
            firebase.auth.GoogleAuthProvider.PROVIDER_ID,
            firebase.auth.FacebookAuthProvider.PROVIDER_ID,
            firebase.auth.EmailAuthProvider.PROVIDER_ID
        ],
        // Terms of service url.
        credentialHelper: firebaseui.auth.CredentialHelper.GOOGLE_YOLO,
        'tosUrl': 'https://www.google.com'
    };

    // Initialize the FirebaseUI Widget using Firebase.
    ui = new firebaseui.auth.AuthUI(firebase.auth());

    ui.start('.modal-body', uiConfig);
    
}]);

myApp.controller('mainController', ['$scope', function ($scope) {

    // $scope.currentUser2 = "";
}]);

myApp.controller('userController', ['$scope', '$http','$sessionStorage', '$mdSidenav', '$mdDialog','fileReader', 'itemService','$location', function ($scope, $http, $sessionStorage, $mdSidenav, $mdDialog, fileReader,
                                                                                                                                           itemService, $location) {

    var idToken = $sessionStorage.get('userToken');
    const userScope = $scope;

    // Get a reference to the storage service, which is used to create references in your storage bucket
    var storage = firebase.storage();
    // Create a storage reference from our storage service
    var storageRef = storage.ref();

    $scope.userText = '';
    $scope.ratingText = '';

    $http({
        method: 'GET',
        url: baseUrl + '/getUserItems',
        headers: {'x-auth': idToken}
    }).then(function successCallBack(response) {

        $scope.userItems = response.data;

    }, function failureCallBack(err) {
        console.log(err);
    })

    $scope.viewItem = function (item) {

        if(item.boardVisibility === 'MainBoard'){
            $scope.userBoardItemDelete = true;
            $scope.userBoardchooseCand = true;
        } else {
            $scope.userBoardItemDelete = false;
            $scope.userBoardchooseCand = false;
        }

        console.log(item);
        $mdSidenav('userMyItems-sidenav').toggle();

        $scope.itemName = item.name;
        $scope.itemPrice = item.price;

        $scope.chooseCandidate = function() {

            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'pages/custom_dialog.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            }).then(function (answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function () {
                $scope.status = 'You cancelled the dialog.';
            });
        };

        $scope.removeItem = function() {

            var confirm = $mdDialog.confirm()
                .title('Confirm candidacy')
                .textContent('Do you really want to remove your item?')
                .ariaLabel('Lucky day')
                .ok('Yes')
                .clickOutsideToClose(true)
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {

                $http({
                    method: 'POST',
                    url: baseUrl + '/deleteItem',
                    data:{itemID: item._id},
                    headers:{'x-auth': idToken}
                }).then(function successCallBack(response) {

                    // Create a reference to the file to delete
                    var desertRef = storageRef.child('/user/'+ item.userID + '/' + item.picture.name);
                    // Delete the file
                    desertRef.delete().then(function() {
                        console.log('deleted image');
                    }).catch(function(error) {
                        console.log(err);
                    });

                    console.log(response.data);

                    function findItemToRemove(element) {
                        return element._id === response.data._id;
                    }

                    var indexToRemove = $scope.userItems.findIndex(findItemToRemove); // the index to remove the item from the array

                    $scope.userItems.splice(indexToRemove, 1);

                    $sessionStorage.put('userItems', $scope.userItems);

                    $mdSidenav('userMyItems-sidenav').toggle(); // toggle sidenav

                }, function failureCallBack(err) {
                    console.log(err);
                })

            }, function() {

            });

        };

        function DialogController($scope, $mdDialog, $http) {

            console.log(userScope);

            $http({
                method: 'POST',
                url: baseUrl + '/getCandidates',
                data:{itemID: item._id},
                headers:{'x-auth': idToken}
            }).then(function successCallBack(response) {

                console.log(response.data);

                if(response.data){
                    $scope.candidatesArr = response.data;

                    if($scope.candidatesArr.length > 0){
                        $scope.title = 'Candidates who wants to buy the item';
                    } else {
                        $scope.title = 'There are no candidates for the item';
                    }
                }

            }, function failureCallBack() {

            })

            $scope.selectCandidate = function (candidate) {

                var confirm = $mdDialog.confirm()
                    .title('Confirm candidacy')
                    .textContent('Do you confirm that ' + candidate.candidateName + ' will buy the item')
                    .ariaLabel('Lucky day')
                    .ok('Yes, I confirm')
                    .cancel('Cancel');

                $mdDialog.show(confirm).then(function() {

                    $http({
                        method: 'POST',
                        url: baseUrl + '/selectCandidate',
                        data:{itemID: item._id, candidateID: candidate.candidateID},
                        headers:{'x-auth': idToken}
                    }).then(function successCallBack(response) {

                        console.log(response.data);
                        userScope.userBoardItemDelete = false;

                    }, function failureCallBack() {

                    })

                }, function() {

                });
            }

            $scope.hide = function() {
                $mdDialog.hide();
            };

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };

        }
    }

    // get item
    if($location.path() === '/updateItem'){
        var item = itemService.getItem();
        $scope.name = item.name;
        $scope.desc = item.description;
        $scope.imageSrc = item.picture.url;
        $scope.price = item.price;
    }

    // user profile
    firebase.auth().onAuthStateChanged(function(user) {
        // update Item
        if (user) {
            $scope.userEmail = user.providerData[0].email;
        } else {
            // No user is signed in.
        }
    });

    $scope.updateEmail = function () {

        $mdDialog.show({
            controller: reAuthenticateDailogCtrl,
            templateUrl: 'pages/re_authDialog.html',
            parent: angular.element(document.body),
            clickOutsideToClose:true,
            fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        })
        .then(function(answer) {

        }, function() {
            $scope.status = 'You cancelled the dialog.';
        });
    }
    
    $scope.updateItem = function () {

        var file = document.getElementById('myFile').files[0];

        if(file){
            firebase.auth().onAuthStateChanged(function(user) {
                if (user) {

                    // Create a reference to the file to delete
                    var desertRef = storageRef.child('/user/'+ user.uid + '/' + item.picture.name);
                        // Delete the file
                    desertRef.delete().then(function() {
                        console.log('deleted image');
                    }).catch(function(error) {
                        // Uh-oh, an error occurred!
                    });

                    var imagesRef = storageRef.child('/user/'+ user.uid +'/'+ file.name);

                    imagesRef.put(file).then(function(snapshot) {
                        console.log('The file has been uploaded');

                        var item = itemService.getItem();

                        $http({
                            method: 'POST',
                            url: baseUrl + '/updateItem',
                            data: {id: item._id, name: $scope.name, price: $scope.price, description: $scope.desc, picture: {url: snapshot.downloadURL, name: file.name} },
                            headers: {'x-auth': idToken}
                        }).then(function successCallBack(response) {
                            $mdDialog.show($mdDialog.alert()
                                .parent(angular.element(document.querySelector('#popupContainer')))
                                .clickOutsideToClose(true)
                                .title('The Item has been updated')
                                .ariaLabel('Alert Dialog Demo')
                                .ok('Dismiss')
                            );
                        }, function failureCallBack(err) {
                            console.log(err);
                        })
                    });
                }
            });
        } else {
            $http({
                method: 'POST',
                url: baseUrl + '/updateItem',
                data: {id: item._id, name: $scope.name, price: $scope.price, description: $scope.desc},
                headers: {'x-auth': idToken}
            }).then(function successCallBack(response) {
                $mdDialog.show($mdDialog.alert()
                    .parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(true)
                    .title('The Item has been updated')
                    .ariaLabel('Alert Dialog Demo')
                    .ok('Dismiss')
                );
            }, function failureCallBack(err) {
                console.log(err);
            })
        }

    }

    $scope.updatePass = function () {

        if($scope.password1 === $scope.password2){

            var user = firebase.auth().currentUser;
            const credential = firebase.auth.EmailAuthProvider.credential(
                user.email,
                $scope.oldPass
            );

            user.reauthenticateWithCredential(credential).then(function() {

                user.updatePassword($scope.password2).then(function() {
                    console.log('password updated');
                    $mdDialog.show($mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('The password has been updated')
                        .textContent('')
                        .ariaLabel('Alert Dialog Demo')
                        .ok('Dismiss')
                    );
                }).catch(function(error) {
                    if(error.code === 'auth/weak-password'){
                        $mdDialog.show($mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('Error')
                            .textContent(error.message)
                            .ariaLabel('Alert Dialog Demo')
                            .ok('Dismiss')
                        );
                    }
                });

            }).catch(function(error) {
                if(error.code === 'auth/wrong-password'){
                    $mdDialog.show($mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('ERROR')
                        .textContent('Your current password is invalid')
                        .ariaLabel('Alert Dialog Demo')
                        .ok('Dismiss')
                    );
                }
            });
        } else {
            $mdDialog.show($mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('ERROR')
                .textContent('Passwords don\'t match')
                .ariaLabel('Alert Dialog Demo')
                .ok('Dismiss')
            );
        }

    }

    // custom dialog for re-auth in order to update email address
    function reAuthenticateDailogCtrl($scope, $mdDialog) {

        $scope.confirmPass = function () {
            $mdDialog.hide();

            var user = firebase.auth().currentUser;

            const credential = firebase.auth.EmailAuthProvider.credential(
                user.email,
                $scope.userPass
            );

            user.reauthenticateWithCredential(credential).then(function() {

                user.updateEmail(userScope.newEmail).then(function() {
                    $mdDialog.show($mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('The email address has been updated')
                        .textContent('')
                        .ariaLabel('Alert Dialog Demo')
                        .ok('Dismiss')
                    );
                }).catch(function(error) {

                    var errorMsg = 'Error';

                    if(error.code === 'auth/email-already-in-use'){
                        errorMsg = 'The email address is already in use.';
                    } else if(error.code === 'auth/invalid-email'){
                        errorMsg = 'Invalid email address';
                    }

                    $mdDialog.show($mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('ERROR')
                        .textContent(errorMsg)
                        .ariaLabel('Alert Dialog Demo')
                        .ok('Dismiss')
                    );

                });

            }).catch(function(error) {

                if(error.code === 'auth/wrong-password'){
                    $mdDialog.show($mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('ERROR')
                        .textContent('Your current password is invalid')
                        .ariaLabel('Alert Dialog Demo')
                        .ok('Dismiss')
                    );
                }
            });
        }

        $scope.hide = function() {
            $mdDialog.hide();
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
            $mdDialog.hide(answer);

            // return Promise.resolve($scope.userPass);
        };
    }


    $scope.$on("fileProgress", function(e, progress) {
        $scope.progress = progress.loaded / progress.total;
    });

    $scope.uploadItem = function () {
        var storage = firebase.app().storage("gs://bitbuy-bitbuy.appspot.com/");

        // Get a reference to the storage service, which is used to create references in your storage bucket
        var storage = firebase.storage();

        // Create a storage reference from our storage service
        var storageRef = storage.ref();

        $scope.imageSrc = "";
        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {

                var file = document.getElementById('myFile').files[0];
                var imagesRef = storageRef.child('/user/'+ user.uid +'/'+ file.name);

                imagesRef.put(file).then(function(snapshot) {
                    console.log('The file has been uploaded');
                    console.log(snapshot.downloadURL);

                    $http({
                        url: baseUrl + '/postItem',
                        method: 'POST',
                        data: {name: $scope.name, price: $scope.price, description: $scope.desc, picture: {url: snapshot.downloadURL, name: file.name} },
                        headers: {'x-auth': idToken}
                    }).then(function successCallBack(response) {

                        var userItems = $sessionStorage.get('userItems');
                        userItems.push(response.data);
                        $sessionStorage.put('userItems', userItems);

                        $mdDialog.show($mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('The Item has been upload')
                            .ariaLabel('Alert Dialog Demo')
                            .ok('Dismiss')
                        );
                    }, function failureCallBack(err) {
                        console.log(err);
                    })
                });

            }
        });
    }

}]);

myApp.directive("ngFileSelect", function(fileReader, $timeout) {
    return {
        scope: {
            ngModel: '='
        },
        link: function($scope, el) {
            function getFile(file) {
                fileReader.readAsDataUrl(file, $scope)
                    .then(function(result) {
                        $timeout(function() {
                            $scope.ngModel = result;
                        });
                    });
            }

            el.bind("change", function(e) {
                var file = (e.srcElement || e.target).files[0];
                getFile(file);
            });
        }
    };
});

myApp.factory("fileReader", function($q, $log) {
    var onLoad = function(reader, deferred, scope) {
        return function() {
            scope.$apply(function() {
                deferred.resolve(reader.result);
            });
        };
    };

    var onError = function(reader, deferred, scope) {
        return function() {
            scope.$apply(function() {
                deferred.reject(reader.result);
            });
        };
    };

    var onProgress = function(reader, scope) {
        return function(event) {
            scope.$broadcast("fileProgress", {
                total: event.total,
                loaded: event.loaded
            });
        };
    };

    var getReader = function(deferred, scope) {
        var reader = new FileReader();
        reader.onload = onLoad(reader, deferred, scope);
        reader.onerror = onError(reader, deferred, scope);
        reader.onprogress = onProgress(reader, scope);
        return reader;
    };

    var readAsDataURL = function(file, scope) {
        var deferred = $q.defer();

        var reader = getReader(deferred, scope);
        reader.readAsDataURL(file);

        return deferred.promise;
    };

    return {
        readAsDataUrl: readAsDataURL
    };
});

myApp.controller('candidateController', ['$scope', '$http','$sessionStorage', '$mdSidenav', '$mdDialog', function ($scope, $http, $sessionStorage, $mdSidenav, $mdDialog) {

    var idToken = $sessionStorage.get('userToken');
    // const userScope = $scope;
    //
    // $scope.userText = '';
    // $scope.ratingText = '';

    $http({
        method: 'POST',
        url: baseUrl + '/getCandidateStatus',
        headers: {'x-auth': idToken}
    }).then(function successCallBack(response) {

        console.log(response.data);
        $scope.candidateItems = response.data;

    }, function failureCallBack(err) {
        console.log(err);
    })

    $scope.viewItem = function (item) {

        $scope.requestStatus = item.candidateStatus;

        if(item.candidateStatus !== 'ACCEPTED'){
            $scope.buyerBoardItemDelete = true;
        } else {
            $scope.buyerBoardItemDelete = false;
        }

        $scope.username = item.username;
        $scope.avgUser = item.userAvg;
        $scope.itemName = item.name;
        $scope.itemPrice = item.price;

        $mdSidenav('userBuyerBoard-sidenav').toggle(); // toggle sidenav

        $scope.chooseCandidate = function() {

            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'pages/custom_dialog.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            }).then(function (answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function () {
                $scope.status = 'You cancelled the dialog.';
            });
        };

        $scope.removeCandidacy = function() {

            var confirm = $mdDialog.confirm()
                .title('Remove candidacy')
                .textContent('Do you really want to remove your candidacy?')
                .ariaLabel('Lucky day')
                .ok('Yes')
                .clickOutsideToClose(true)
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {

                console.log(item._id);
                console.log(idToken);

                $http({
                    method: 'POST',
                    url: baseUrl + '/deleteCandidacy',
                    data:{itemID: item._id},
                    headers:{'x-auth': idToken}
                }).then(function successCallBack(response) {

                    console.log(response.data);

                    function findItemToRemove(element) {
                        return element._id === response.data._id;
                    }

                    var indexToRemove = $scope.candidateItems.findIndex(findItemToRemove); // the index to remove the item from the array

                    $scope.candidateItems.splice(indexToRemove, 1);

                }, function failureCallBack(err) {
                    console.log(err);
                })

            }, function() {

            });

        };

        function DialogController($scope, $mdDialog, $http) {

            console.log(userScope);

            $http({
                method: 'POST',
                url: baseUrl + '/getCandidates',
                data:{itemID: item._id},
                headers:{'x-auth': idToken}
            }).then(function successCallBack(response) {

                console.log(response.data);

                if(response.data){
                    $scope.candidatesArr = response.data;

                    if($scope.candidatesArr.length > 0){
                        $scope.title = 'Candidates who wants to buy the item';
                    } else {
                        $scope.title = 'There are no candidates for the item';
                    }
                }

            }, function failureCallBack() {

            })

            $scope.selectCandidate = function (candidate) {

                var confirm = $mdDialog.confirm()
                    .title('Confirm candidacy')
                    .textContent('Do you confirm that ' + candidate.candidateName + ' will buy the item')
                    .ariaLabel('Lucky day')
                    .ok('Yes, I confirm')
                    .cancel('Cancel');

                $mdDialog.show(confirm).then(function() {

                    $http({
                        method: 'POST',
                        url: baseUrl + '/selectCandidate',
                        data:{itemID: item._id, candidateID: candidate.candidateID},
                        headers:{'x-auth': idToken}
                    }).then(function successCallBack(response) {

                        console.log(response.data);
                        userScope.userBoardItemDelete = false;

                    }, function failureCallBack() {

                    })

                }, function() {

                });
            }


            $scope.hide = function() {
                $mdDialog.hide();
            };

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };

        }
    }

}])

myApp.controller('secondController', ['$scope','$http', '$sessionStorage', '$mdDialog', 'Reddit', function ($scope, $http, $sessionStorage, $mdDialog, Reddit) {

    console.log(window.innerHeight);
    var item = document.getElementById('itemsScreen');

    item.style.height = (window.innerHeight - 170)+ 'px';

    $scope.reddit = new Reddit();
    $scope.loadmore = true;
    $scope.getItemsScroll = function(){
        var items = $scope.reddit.nextPage($scope, item);
        console.log(items);
        $scope.$parent.itemsArr = items;
    }

    // $http({
    //     method : "GET",
    //     url : "http://localhost:3000/getitems"
    // }).then(function success(response) {
    //
    //     console.log(response.data);
    //
    //     // $scope.$parent.itemsArr = response.data.filter( item => item.visible === true);
    //
    //     $scope.$parent.itemsArr = response.data;
    //
    // }, function failure(err) {
    //
    // });

}]);

myApp.factory('Reddit', function($http) {

    var Reddit = function() {
        this.items = [];
        this.busy = false;
        this.after = 1;
        this.finish = false;
    };

    Reddit.prototype.nextPage = function($scope, item) {
        console.log(this.after);
        if (this.busy) {
            return;
        }

        var url = baseUrl + '/getitems';
        var globalReddit = this;
        // console.log(globalReddit.after);
        if(! this.finish){
            $http({
                method: 'GET',
                url: url,
                params: {page: globalReddit.after}
            }).then(function successCB(response){
                // console.log(response.data);
                var items = response.data.docs;

                for (var i = 0; i < items.length; i++) {
                    globalReddit.items.push(items[i]);
                }
                // console.log(globalReddit.items);
                if(response.data.page < response.data.pages){
                    globalReddit.after += 1;
                }

                if(globalReddit.items.length === response.data.total){
                    globalReddit.finish = true;
                    $scope.loadmore = false;
                    item.style.height = (window.innerHeight - 100)+ 'px';
                }

                //   console.log('after: ', this.after);
                this.busy = false;
            }, function failureCB(err){
                console.log(err);
            });
        }

        return globalReddit.items;

    };

    return Reddit;
});

myApp.directive("userItems", function() {

    return {
        // template : "<h1>{{ i.name }} , {{ i.price }}</h1>"
        templateUrl: 'directives/thumbnail-item.html'
    };
});
